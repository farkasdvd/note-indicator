/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * note-indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QString>

class Note: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString guid READ guid)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)

public:
    Note(const QString& guid, const QString& title);

    const QString& guid() const;
    const QString& title() const;
    void setTitle(const QString& title);

signals:
    void titleChanged();

private:
    QString m_guid;
    QString m_title;
};
