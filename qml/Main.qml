/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * note-indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Example 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'note-indicator.farkasdvd'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings

        property string noteGuid: ""
    }

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr("Select note")
        }

        UbuntuListView {
            anchors.top: header.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            currentIndex: -1
            model: notes
            delegate: ListItem {
                height: _content.height
                action: Action {
                    onTriggered: {
                        print("selected " + model.guid)
                        settings.noteGuid = model.guid
                    }
                }

                ListItemLayout {
                    id: _content
                    title.text: model.title
                    subtitle.text: model.guid

                    Icon {
                        SlotsLayout.overrideVerticalPositioning: true
                        SlotsLayout.position: SlotsLayout.Trailing
                        anchors.verticalCenter: parent.verticalCenter
                        width: units.gu(3)
                        height: width
                        name: "tick"
                        visible: model.guid == settings.noteGuid
                    }
                }
            }
        }
    }
}
