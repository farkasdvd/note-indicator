#!/bin/bash

set -e

rm /home/phablet/.config/upstart/farkasdvd-indicator-note.conf
rm /home/phablet/.local/share/unity/indicators/com.farkasdvd.indicator.note

echo "note-indicator uninstalled"
