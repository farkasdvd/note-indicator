#!/bin/bash

set -e

mkdir -p /home/phablet/.config/upstart/
mkdir -p /home/phablet/.local/share/unity/indicators/

cp -v /opt/click.ubuntu.com/note-indicator.farkasdvd/current/indicator/farkasdvd-indicator-note.conf /home/phablet/.config/upstart/
cp -v /opt/click.ubuntu.com/note-indicator.farkasdvd/current/indicator/com.farkasdvd.indicator.note /home/phablet/.local/share/unity/indicators/

echo "note-indicator installed!"
