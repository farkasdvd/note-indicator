/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * note-indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QString>
#include <QSettings>

#include "notes.h"
#include "note.h"

namespace
{
    const QString c_dirPath = "/home/phablet/.local/share/com.ubuntu.reminders/@local/";
    const QString c_cacheFileName = "notes.cache";
    const QString c_fileNamePrefix = "note-";
    const QString c_fileNameSuffix = ".info";
    const QString c_paramGuid = "guid";
    const QString c_paramTitle = "title";
}

Notes::Notes()
    : QAbstractListModel(nullptr)
    , m_notes()
{
    QSettings cacheFile(c_dirPath + c_cacheFileName, QSettings::IniFormat);
    cacheFile.beginGroup("notes");

    for(auto&& key : cacheFile.allKeys())
    {
        QSettings noteFile(c_dirPath + c_fileNamePrefix + key + c_fileNameSuffix, QSettings::IniFormat);

        m_notes.push_back(new Note(key, noteFile.value(c_paramTitle).toString()));
    }

    qDebug() << "Found" << m_notes.size() << "notes in" << c_dirPath;
}

int Notes::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_notes.count();
}

QVariant Notes::data(const QModelIndex &index, int role) const
{
    switch(role)
    {
        case Notes::RoleGuid:
            return m_notes.at(index.row())->guid();
        case Notes::RoleTitle:
            return m_notes.at(index.row())->title();
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> Notes::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(RoleGuid, "guid");
    roles.insert(RoleTitle, "title");

    return roles;
}
