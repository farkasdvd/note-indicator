/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * note-indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "note.h"

Note::Note(const QString& guid, const QString& title)
    : QObject(nullptr)
    , m_guid(guid)
    , m_title(title)
{
    // intentionally empty
}

const QString& Note::guid() const
{
    return m_guid;
}

const QString& Note::title() const
{
    return m_title;
}

void Note::setTitle(const QString& title)
{
    m_title = title;
}
